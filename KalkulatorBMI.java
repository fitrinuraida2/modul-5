import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class KalkulatorBMI extends JFrame
{
    private final JTextField textmass = makePrettyTextField();
    private final JTextField textHeight = makePrettyTextField();
    private final JButton btnCalc = makePrettyButton("Calculate BMI");
    private final KalkulatorBMI self = this;
    //Konstruktor
    public KalkulatorBMI(){
        super();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Kalklator BMI");
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
        textmass.setPreferredSize(new Dimension(200, 30));
        textHeight.setPreferredSize(new Dimension(200, 30));
        textmass.setMaximumSize(textmass.getPreferredSize());
        textHeight.setMaximumSize(textHeight.getPreferredSize());
        getContentPane().setBackground(new Color(232,240,255));
        getContentPane().add(makePrettyLabel("Your mass(kg) : "));
        getContentPane().add(Box.createRigidArea(new Dimension(5, 5)));
        getContentPane().add(textmass);
        getContentPane().add(Box.createRigidArea(new Dimension(5, 5)));
        getContentPane().add(Box.createVerticalGlue());
        getContentPane().add(makePrettyLabel("Your Height(cm) :"));
        getContentPane().add(Box.createRigidArea(new Dimension(5, 5)));
        getContentPane().add(textHeight);
        getContentPane().add(Box.createRigidArea(new Dimension(5, 5)));
        getContentPane().add(Box.createVerticalGlue());
        getContentPane().add(btnCalc);
        getContentPane().add(Box.createRigidArea(new Dimension(5, 5)));
        
        //membuat kalkulasi
        btnCalc.addActionListener(new ActionListener(){
        
            @Override
            public void actionPerformed(ActionEvent e) {
                double mass;
                double height;
                try{
                    mass = Double.parseDouble(textmass.getText());
                    height = Double.parseDouble(textHeight.getText());
                }catch(NumberFormatException nfe){
                    JOptionPane.showMessageDialog(self,"Masukan nilai Berat dan Tinggi yang valid","Input Error",JOptionPane.ERROR_MESSAGE);
                    return;
                }
                double result = kalkulasiBmi(mass,height);
                JOptionPane.showMessageDialog(self,"Nilai BMI anda : "+(Math.round(result*100.0)/100),"Nilai BMI anda",JOptionPane.PLAIN_MESSAGE);
            }
        });
        pack();
        setVisible(true);
    }

    protected double kalkulasiBmi(double mass,double height){
        return mass/Math.pow(height/100.0,2.0);
    }
    private JButton makePrettyButton(String title){
        JButton button = new JButton(title);
        button.setFont(new Font(Font.SANS_SERIF, Font.PLAIN,16));
        button.setBorder(BorderFactory.createRaisedBevelBorder());
        button.setBackground(Color.white);
        button.setBackground(new Color(53,124,255));
        return button;
    }

    private JTextField makePrettyTextField(){
        JTextField field = new JTextField();
        field.setFont(new Font(Font.SANS_SERIF, Font.ITALIC,16));
        field.setHorizontalAlignment(JTextField.RIGHT);
        field.setBorder(BorderFactory.createLoweredBevelBorder());
        return field;
    }

    private JLabel makePrettyLabel(String title){
        JLabel label = new JLabel(title);
        label.setFont(new Font(Font.SANS_SERIF,Font.BOLD,14));
        label.setForeground(new Color(53,124,255));
        return label;
    }

    //main
    public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable(){
        
            @Override
            public void run() {
                new KalkulatorBMI();
            }
        });
    }
}
