import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

///// kelas GameFrames
public class GameFrames{

    ///////////////////////////////////////////////
    ///// Deklarasi komponen 
    ///////////////////////////////////////////////
    protected JFrame frame; ///// frame game
    protected Container contentPane; ///// kontainer untuk meletakkan panel 
    protected JPanel pnlNorth; ///// untuk meletakkan komponen menu bar
    protected JPanel pnlCenter; ///// untuk meletakkan komponen button 1-9
    protected JPanel pnlSouth; ///// untuk meletakkan label giliran permainan
    protected JMenuBar menuBar; ///// menu bar
    protected JMenu menu; ///// untuk meletakkan menu
    protected JMenuItem itemReset; ///// untuk reset game
    protected JMenuItem itemInfo; ///// untuk info game
    protected JMenuItem itemExit; ///// untuk keluar game
    protected JButton btn1; ///// tombol 1
    protected JButton btn2; ///// tombol 2
    protected JButton btn3; ///// tombol 3
    protected JButton btn4; ///// tombol 4
    protected JButton btn5; ///// tombol 5
    protected JButton btn6; ///// tombol 6
    protected JButton btn7; ///// tombol 7
    protected JButton btn8; ///// tombol 8
    protected JButton btn9; ///// tombol 9
    protected JButton btnTemp; ///// tombol kosong
    protected JLabel lblTurn; ///// label giliran permainan
    ///////////////////////////////////////////////
    
    public GameFrames(String title) {
        ///// set judul game
        frame = new JFrame(title);
        ///// mendapatkan container dari awt
        contentPane = frame.getContentPane();
        
        ///// method panelComponent untuk komponen panel
        panelComponent();
        ///// method menuComponent untuk komponen menu
        menuComponent();
        ///// method buttonComponent untuk komponen tombol
        buttonComponent();
        ///// method labelComponent untuk komponen label
        labelComponent();
    }

    ///// method menuComponent
    protected void menuComponent() {
        ///// menuBar untuk meletakkan menu utama
        menuBar = new JMenuBar();
        
        ///// menu utama (File)
        menu = new JMenu("File");
        ///// shortcut menu Alt+F
        menu.setMnemonic(KeyEvent.VK_F);
        ///// tooltip saat pointer menunjuk menu utama
        menu.setToolTipText("Menu pilihan...");
        
        ///// menu reset game
        itemReset = new JMenuItem("Reset", 1);
        ///// shortcut reset game Alt+R 
        itemReset.setMnemonic(KeyEvent.VK_R);
        ///// tooltip saat pointer menunjuk menu reset
        itemReset.setToolTipText("Reset Game");
        
        ///// menu info game
        itemInfo = new JMenuItem("Info", 2);
        ///// shortcut info game Alt+I
        itemInfo.setMnemonic(KeyEvent.VK_I);
        ///// tooltip saat pointer menunjuk menu info
        itemInfo.setToolTipText("Info Game");
        
        ///// menu exit game
        itemExit = new JMenuItem("Exit", 3);
        ///// shortcut exit game Alt+E
        itemExit.setMnemonic(KeyEvent.VK_E);
        ///// tooltip saat pointer menunjuk menu exit
        itemExit.setToolTipText("Exit Game");
    }

    ///// method buttonComponent
    protected void buttonComponent() {
        btn1 = new JButton();
        btn2 = new JButton();
        btn3 = new JButton();
        btn4 = new JButton();
        btn5 = new JButton();
        btn6 = new JButton();
        btn7 = new JButton();
        btn8 = new JButton();
        btn9 = new JButton();
        btnTemp = new JButton();
    }
    
    ///// method labelComponent
    protected void labelComponent() {
        lblTurn = new JLabel();
    }
    
    ///// method panelComponent
    protected void panelComponent() {
        pnlNorth = new JPanel();
        pnlCenter = new JPanel();
        pnlSouth = new JPanel();
    }
    
    ///// method launchFrame
    public void launchFrame(){
        ///// panel utara
        panelNorth();
        ///// panel tengah
        panelCenter();
        ///// panel selatan
        panelSouth();
        
        ///// menambahkan panel ke kontainer
        contentPane.add(pnlNorth, BorderLayout.NORTH);
        contentPane.add(pnlCenter, BorderLayout.CENTER);
        contentPane.add(pnlSouth, BorderLayout.SOUTH);
        
        ///// tidak bisa dirubah ukuran window nya
        frame.setResizable(false);
        ///// set ukuran 300x300
        frame.setSize(300, 300);
        ///// terlihat
        frame.setVisible(true);
    }

    ///// method panelNorth
    protected void panelNorth() {
        ///// set panel layout dengan flow layout
        ///// align = left
        ///// hgap = vgap = 3
        pnlNorth.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 3));
        ///// tambahkan menu reset ke menu utama
        menu.add(itemReset);
        ///// tambahkan menu info ke menu utama
        menu.add(itemInfo);
        ///// tambahkan menu exit ke menu utama
        menu.add(itemExit);
        ///// tambahkan menu utama ke menu bar
        menuBar.add(menu);
        ///// tambahkan menu bar ke panel utara
        pnlNorth.add(menuBar);
    }

    ///// method panelCenter
    protected void panelCenter() {
        ///// set panel layout dengan grid layout
        ///// ukuran 3x3
        ///// hgap = vgap = 3
        pnlCenter.setLayout(new GridLayout(3, 3, 3, 3));
        ///// warna background = 238, 238, 238(R,G,B)
        pnlCenter.setBackground(new Color(238, 238, 238));
        ///// tambahkan tombol 1 - 9 ke panel tengah
        pnlCenter.add(btn1);
        pnlCenter.add(btn2);
        pnlCenter.add(btn3);
        pnlCenter.add(btn4);
        pnlCenter.add(btn5);
        pnlCenter.add(btn6);
        pnlCenter.add(btn7);
        pnlCenter.add(btn8);
        pnlCenter.add(btn9);
    }

    ///// method panelSouth
    protected void panelSouth() {
        ///// set panel layout dengan flow layout
        ///// align = center
        pnlSouth.setLayout(new FlowLayout(FlowLayout.CENTER));
        ///// tambahkan label giliran ke panel selatan
        pnlSouth.add(lblTurn);
    }
}