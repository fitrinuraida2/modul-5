import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
public class SwingApplication extends JFrame implements ActionListener{
	private int numClick = 0;
	private JLabel lb1;
	private JButton btn1;	
	public SwingApplication() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(2,1));		
		lb1 = new JLabel("0");
		lb1.setHorizontalAlignment(JLabel.CENTER);
		add(lb1);		
		btn1 = new JButton("I'm Swing Button");
		btn1.addActionListener(this);
		add(btn1);		
		pack();
		setVisible(true);
	}	
	public void actionPerformed(ActionEvent e) {
		numClick++;
		lb1.setText(String.valueOf(numClick));
	}	
	public static void main(String[] args) {
		new SwingApplication();
	}
}
