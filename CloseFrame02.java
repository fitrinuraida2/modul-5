import java.awt.*;
import java.awt.event.*;

public class CloseFrame02 extends Frame implements ActionListener
{
    Label label;
    Button b1;
    
    public CloseFrame02(String title){
        super(title);
        label = new Label("Tutup Frame!");
        this.b1 = new Button("Tutup Frame");
        b1.addActionListener(this);
        add(b1,BorderLayout.NORTH);
        this.addWindowListener(new CFListener());        
    }

    public void launchFrame(){
        setSize(300,300);
        setVisible(true);
    }

    public void actionPerformed(ActionEvent e){
        this.addWindowListener(new CFListener());
    }
    // Inner Class
    class CFListener extends WindowAdapter
    {
        public void windowClosing(WindowEvent e){
             dispose();
             System.exit(1);
        }
    }
   
    public static void main(String[] args){
        CloseFrame02 close = new CloseFrame02("Coba Close Window with windowAdapter");
        close.launchFrame();
    }
}
